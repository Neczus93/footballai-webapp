package hu.footballai.services;

import java.util.Date;
/**
 * @author Neczpal Oliver
 * Description: This program see the football prediction.
 * 
 */
 
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import hu.footballai.model.FootballMatch;

@Path("/prediction")
public class PredictionService {
	@GET
	@Produces("application/json")
	public Response getPrediction() throws JSONException {
		JSONArray jsonArray = new JSONArray();
		
		JSONObject fbFootballMatch1 = new FootballMatch(new Date(),"homeTeam1", "awayTeam1", "V", 86).getAsJSONObject();
		JSONObject fbFootballMatch2 = new FootballMatch(new Date(),"homeTeam2", "awayTeam2", "V", 79).getAsJSONObject();
		JSONObject fbFootballMatch3 = new FootballMatch(new Date(),"homeTeam3", "awayTeam3", "H", 91).getAsJSONObject();
		JSONObject fbFootballMatch4 = new FootballMatch(new Date(),"homeTeam4", "awayTeam4", "H", 82).getAsJSONObject();
		JSONObject fbFootballMatch5 = new FootballMatch(new Date(),"homeTeam5", "awayTeam5", "V", 74).getAsJSONObject();
		
		jsonArray.put(fbFootballMatch1);
		jsonArray.put(fbFootballMatch2);
		jsonArray.put(fbFootballMatch3);
		jsonArray.put(fbFootballMatch4);
		jsonArray.put(fbFootballMatch5);
 
		String result = String.valueOf(jsonArray);
		return Response.status(200).entity(result).build();
	}
}