package hu.footballai.model;

import java.io.Serializable;
import java.util.Date;

import org.json.JSONObject;

public class FootballMatch implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9031647931038231693L;
	private Date date;
	private String homeTeam;
	private String awayTeam;
	private String predictionResult;
	private int predictionChance;
	
	public JSONObject getAsJSONObject() {
		JSONObject retJSON = new JSONObject();
		
		retJSON.put("date", this.date);
		retJSON.put("homeTeam", this.homeTeam);
		retJSON.put("awayTeam", this.awayTeam);
		retJSON.put("predictionResult", this.predictionResult);
		retJSON.put("predictionChance", this.predictionChance);
		
		return retJSON;
	}
	
	public FootballMatch() {
		super();
	}
	
	public FootballMatch(Date date, String homeTeam, String awayTeam, String predictionResult, int predictionChance) {
		super();
		this.date = date;
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.predictionResult = predictionResult;
		this.predictionChance = predictionChance;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}

	public String getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(String awayTeam) {
		this.awayTeam = awayTeam;
	}

	public String getPredictionResult() {
		return predictionResult;
	}

	public void setPredictionResult(String predictionResult) {
		this.predictionResult = predictionResult;
	}

	public int getPredictionChance() {
		return predictionChance;
	}

	public void setPredictionChance(int predictionChance) {
		this.predictionChance = predictionChance;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{").append("\"").append("date").append("\"").append("=");
		builder.append(date);
		builder.append(", \"homeTeam\"=");
		builder.append(homeTeam);
		builder.append(", \"awayTeam\"=");
		builder.append(awayTeam);
		builder.append(", \"predictionResult\"=");
		builder.append(predictionResult);
		builder.append(", \"predictionChance\"=");
		builder.append(predictionChance);
		builder.append("}");
		return builder.toString();
	}
}
